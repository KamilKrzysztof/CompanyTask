package com.company;

public class Model{
    private long result;
    private long first_element;
    private long second_element;

    public Model(long result, long first_element, long second_element) {
        this.result = result;
        this.first_element = first_element;
        this.second_element = second_element;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    public long getFirst_element() {
        return first_element;
    }

    public void setFirst_element(long first_element) {
        this.first_element = first_element;
    }

    public long getSecond_element() {
        return second_element;
    }

    public void setSecond_element(long second_element) {
        this.second_element = second_element;
    }

}
